#!/usr/bin/env python
import socket, sys

try:
    HOST = sys.argv[1]
    PORT = int(sys.argv[2])
except ValueError as e:
     print "I/O error: "
     print(e)
except:
    HOST, PORT = "192.168.100.233", 9020

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
input = ""

try:
    sock.connect((HOST, PORT))
    print sock.recv(1024)
    while input != "adios":
        input = raw_input("> ")
        sock.sendall(input + "\r\n")
        received = sock.recv(1024)
        print received.strip()
except KeyboardInterrupt:
    print "\r\nExiting. . .\r\n"
finally:
    sock.close()
    sock = None

