#!/usr/bin/env python
import socket
import threading
import SocketServer
from datetime import datetime
class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        self.request.sendall("Welcome to Brice's Chat Room\r\n");
        data = threading.local()
        data.name = "guest"
	log.push(data.name, "CONNECTED")
        while 1:
            data.recv = self.request.recv(1024).strip()
            if (data.recv.startswith("help")):
                self.request.sendall("All Commands:\r\n\thelp: show all commands\r\n\ttest <words>: receives a response of 'words'\r\n\tname <name>: sets your username\r\n\t get returns the entire chat log\r\n\tpush: <words> adds words to the chat buffer\r\n\t getrange <start> <end> gets a range of entries from the chat buffer\r\n\tadios exits chatroom\r\n")

            elif (data.recv.startswith("test: ")):
                self.request.sendall(data.recv[6:]+"\r\n")

            elif (data.recv.startswith("name: ")):
                data.name = data.recv[6:].strip()
                log.push(data.name, "SET NAME")
                self.request.sendall("OK\r\n");

            elif (data.recv.startswith("getrange")):
                try:
                    params = data.recv.split(" ")
                    tmp = log.printRange(int(params[1]), int(params[2]))
                    self.request.sendall(tmp)
                except ValueError:
                    self.request.sendall('error: invalid range')

            elif (data.recv.startswith("get")):
                self.request.sendall(log.printLog());

            elif (data.recv.startswith("push: ")):
                newData = data.recv[6:].strip()
                log.push(data.name, newData)
                self.request.sendall("OK")

            elif (data.recv == "clear"):
                log.clearLog()
                self.request.sendall("OK")

            elif (data.recv.startswith("adios")):
                log.push(data.name, "DISCONNECTED")
                break
            else:
		try:
			self.request.sendall("Invalid Command")
		except:
			break
            # print "{} wrote:".format(self.client_address[0])
            # print data.recv

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

class Log:
    u = []
    def push(self, user, entry):
        row = user, str(datetime.now()), entry
        self.u.append(row)
        print row[0] +" \t "+ row[1] +" \t "+ row[2]
    def printLog(self):
        output = ""
        if self.u.__len__():
            for row in self.u:
                output += row[0] +"\t"+ row[1] +"\t"+ row[2] + "\r\n"
            return output
        else:
            return "Chat log empty"
    def printRange(self, start, end):
        output = ""
        try:
            for row in self.u[start:end]:
                output += row[0] +"\t"+ row[1] +"\t"+ row[2] + "\r\n"
        except:
            output = "error: invalid range"
        finally:
            return output
    def clearLog(self):
        del self.u[:]



if __name__ == "__main__":
    HOST, PORT = "", 9020
    server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    server.allow_reuse_address = True;
    ip, port = server.server_address
    log = Log()

    input = ""

    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.daemon = True
    server_thread.start()
    print "Server running in loop in thread: " + server_thread.name
    try:
        while input != "quit":
            hi = 1
    except KeyboardInterrupt:
        server.shutdown()
